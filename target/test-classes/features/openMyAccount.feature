Feature: Open Your Own Account
  Narrative:
  As an user Choose Bank Account, Give details, Identify, Close Questions and confirm your details to Open Your Own Account.

  Background:
    Given User opens "Open Your Account Url"
    Then User accept cookie when needed

  @end2End @regressionTest @smokeTest
  Scenario: End to End flow- Open Your Own Account
      # 1 Choose Bank Account
    Then verify the banner content "PRIVACY_BANNER_CHOOSE_BANK_ACC"
    When User select "private" bank Account "for myself" living "Netherlands"
    Then verify the banner content "INFO_BANNER_CHOOSE_BANK_ACC"
    And User enter the postalCode "1082 PP" and houseNumber "10"
    Then Verify the streetName "Gustav Mahlerlaan" and Town "Amsterdam" is displayed correctly
    When User Click "Tell us who you are"
      # 2 Our details
    Then Verify user is in "Your details" page
    When User enters the salutation,initials,firstName,surname and dateOfBirth
    And User not select born in USA
    And User enter "valid" BSN 999999990 and Netherlands tax payer "Yes"
    When User Click "Identification"
      # 3 Identification
    Then Verify user is in "Identification" page
    When User selects the "Valid" ID proof and given "Valid" Email Phone Number
    And User checks streetName given in Previous page "Gustav Mahlerlaan"
    When User Click "To Closing Questions"
      # 4 Closing Questions
    Then Verify user is in "Closing Questions" page
    Then User "Valid" response to "Real Estate" income questions
    Then User Click "Overview"

      # 5 Confirmation
    Then Verify user is in "Confirmation" page
    And Verify the Basic package cost is "1,95"
    And Verify the name
    And Verify the Address is "Gustav Mahlerlaan 10 1082 PP Amsterdam"
    And Verify the Date of Birth
    And Verify the TelephoneNumber
    And Verify the EmailAddress
    And PDF links are verified for "BASIC_PAYMENT,RETAIL_PAYMENT,DEPOSITOR"
    And Verify the "CHOOSE_BANK_ACC,YOUR_DETAILS,IDENTIFICATION,CLOSING_QUESTIONS,CONFIRMATION" screens have highlighted texts
    Then User Click "Apply Now"

    # 6 Thank you
    Then Verify user is in "Thank You" page
    Then User verifies confirmation is send to correct email

  @end2End @regressionTest @smokeTest
  Scenario Outline: Users not able to create online Account using this flow
    When User select "<accountType>" bank Account "for myself" living "<livingIn>"
    Then verify the banner content "<banner>"
    Examples:
      | accountType | livingIn    | banner             |
      | private     | USA         | NOT_IN_NETHERLANDS |
      | business    | Netherlands | BUSINESS_ACCOUNT   |

  @regressionTest @component @smokeTest
  Scenario Outline: Error message validations in Choose Bank Account Screen
    When User select "private" bank Account "for myself" living "Netherlands"
    And User enter the postalCode "<postCode>" and houseNumber "<houseNumber>"
    Then Verify that error message "<errorMessage>" is identified in "<errorFlow>" for "<errorField>" of "ChooseBankAccount" page
    Examples:
      | postCode | houseNumber | errorFlow          | errorMessage        | errorField               |
      |          |             | emptyInput         | BLANK_ERROR_MESSAGE | POSTAL_CODE,HOUSE_NUMBER |
      | 1q23h    |             | invalidPostalCode  | INVALID_POSTAL_CODE | POSTAL_CODE              |
      |          | aq          | invalidHouseNumber | INVALID_HOUSE_N0    | HOUSE_NUMBER             |

  @component @debug
  Scenario Outline: Error message validations for "<errorField>" in "Your Details Screen"
    When User select "private" bank Account "for myself" living "Netherlands"
    And User enter the postalCode "1082 PP" and houseNumber "10"
    Then Verify the streetName "Gustav Mahlerlaan" and Town "Amsterdam" is displayed correctly
    When User Click "Tell us who you are"
    Then Verify user is in "Your details" page
    When User enter "<errorFlow>" initials "<initials>", dob "<dob>" and dutch personal id "<dutchId>"
    When User Click "Identification"
    Then Verify that error message "<errorMessage>" is identified in "<errorFlow>" for "<errorField>" of "yourDetails" page
    Examples:
      | errorFlow              | errorMessage          | errorField               | initials | dob        | dutchId   |
      | empty                  | BLANK_ERROR_MESSAGE   | BLANK_INITIALS,BLANK_DOB |          |            | 999999990 |
      | invalid initials       | INVALID_INITIAL_ERROR | INVALID_INITIALS         | qwerty   | 25-12-2000 | 999999990 |
      | invalid dutchId        | INVALID_DUTCH_ID      | INVALID_PERSONAL_ID      | q        | 25-12-2000 | 999999    |
      | dob less than 18 years | INVALID_AGE_ERROR     | AGE_DOB                  | q        | 25-12-2012 | 999999990 |
      | invalid dob in future  | INVALID_DOB_ERROR     | FUTURE_DOB               | q        | 25-12-2024 | 999999990 |

  @todo
  Scenario: Check the content on information icon

  @todo
  Scenario: Check moving to previous pages is possible

  @todo
  Scenario: Detailed UI verification

