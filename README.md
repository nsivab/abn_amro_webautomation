# ABNAMRO_Assignment

## Description
Created the auto test for private user account creation for NL user living in "1082PP Gustav Maherlaan 10 Amsterdam". Few non-happy flows are covered for Eligibility check, validation check,Banner content checks.
Tests are written in BDD, Scenarios is easily understandable and steps are clearly defined. Tests can be executed based on tags.
While Automating found some stability issues, which is quite challenging.
Tests are integrated with gitlab.For each committed changes,pipeline job will be triggered in gitlab.Also scheduler runs at 8 every day.
Below, explained the basic installation steps and links to follow.

*Note- I have used "@smokeTest" tag to execute selected scenarios from list of scenarios in Feature file.*

#### Motivation for choosing this framework
I have used an Open Source (Selenium- CuCumber BDD Framework in Java) which benefits below points,
   1. Easy to Understand- Anyone in the Organisation can easily understand the Application.
   2. Clarity- This approach also reduce a gap between Tester, Developers and Business Analyst.
   3. Adaptability- This framework is easy to update as the product requirement changes in the future.
   4. Supportability- Framework can also be implemented in other languages like Java, Python etc.

## Installation
### Local installations
#### Install IDE Intellij
https://www.jetbrains.com/idea/download/
* Use the Community version
* Download and follow the instructions for install
#### Install Git
https://git-scm.com/downloads
#### Install Java
https://devqa.io/brew-install-java/
* Use brew cask install adoptopenjdk8
> java -version ---> use it to confirm if Java8 installed successfully
#### Install Maven
> brew install maven
#### Setup Intellij
* Open the IDE
* Open project
* Select the project cloned from repo
* Ensure jdk is configured correctly under project setting(cmd+;)
* Build the project--> Right Click project --> open in terminal
> mvn clean install
* This will build the project and run the tests from testsuite
#### GitLab integration setup
Please follow the instructions:
- [ ] [install gitlab runner](https://docs.gitlab.com/runner/install/osx.html)
- [ ] [register the runner](https://docs.gitlab.com/runner/register/index.html)
* For every change in your repo test will be triggered
* Test results will be available in artifacts for 2 days

#### Project Structure
* genericComponents --> *src/main/java/generic/components/* ---> Utilities
* enumModels --> *src/main/java/generic/models* ---> Enumerated constant values
* pageObjects --> *src/test/java/pageObjects* ---> place for locators
* stepsDefinitions --> *src/test/java/stepDefinitions* ---> stepDefinitions used for BDD
* testRunner --> *src/test/java/testRunner* ---> runner setup
* features --> *src/test/resources/features* ---> Feature file is located
* properties --> *src/test/resources/properties* ---> property file to retrieve url and other auth data
* testOutput --> *test-report* --->FileName:test-output.html
* pom.xml --> *pom.xml* ---> maven dependencies
* yaml --> *root path* ---> Pipeline script to run the tests from Gitlab
