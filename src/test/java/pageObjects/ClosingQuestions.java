package pageObjects;

import generic.components.UtilityClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.text.ParseException;

public class ClosingQuestions extends UtilityClass {

    public ClosingQuestions(WebDriver driver) throws IOException {
        super();
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 60);
    }

    @FindBy(id = "P388-C2-C0-C0-TI1")
    @CacheLookup
    WebElement closingPageTitle;

    @FindBy(id = "P389-C2-C1-B1")
    @CacheLookup
    WebElement closingPageButton;

    @FindBy(id = "P388-C2-C0-C0-C2-F1-2")
    @CacheLookup
    WebElement naCheckbox;

    @FindBy(id = "P388-C2-C0-C0-C2-F1-0")
    @CacheLookup
    WebElement realEstateCheckbox;

    @FindBy(id = "P388-C2-C0-C0-C2-F1-1")
    @CacheLookup
    WebElement cryptoCheckbox;

    @FindBy(id = "P388-C2-C0-C0-C2-F2-1")
    @CacheLookup
    WebElement no1Button;

    @FindBy(id = "P388-C2-C0-C0-C2-F3-0")
    @CacheLookup
    WebElement yes2Button;

    @FindBy(id = "P388-C2-C0-C0-C2-F3-1")
    @CacheLookup
    WebElement no3Button;

    @FindBy(id = "P388-C2-C0-C0-C2-F4-1")
    @CacheLookup
    WebElement no4Button;

    @FindBy(id = "P388-C2-C0-C0-C2-F6-1")
    @CacheLookup
    WebElement no5Button;

    @FindBy(id = "P388-C2-C0-C0-C2-F7-1")
    @CacheLookup
    WebElement no6Button;

    @FindBy(id = "P388-C2-C1-B1")
    @CacheLookup
    WebElement overviewButton;

    @FindBy(id = "P388-C2-C1-B0")
    @CacheLookup
    WebElement YourIdentificationBackButton;

    public Boolean getClosingPageTitle() {

        return closingPageTitle.isDisplayed();
    }

    public void selectNACheckbox() {

        naCheckbox.click();
    }

    public void selectRealEstateCheckbox() {

        realEstateCheckbox.click();
    }

    public void selectCryptoCheckbox() {

        cryptoCheckbox.click();
    }

    public void selectN01Button() throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P388-C2-C0-C0-C2-F2-1")));
        no1Button.click();
    }

    public void selectYes2Button() {
        yes2Button.click();
    }

    public void selectN03Button() throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P388-C2-C0-C0-C2-F3-1")));
        no3Button.click();
    }

    public void selectN04Button() {
        no4Button.click();
    }

    public void selectN05Button() throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P388-C2-C0-C0-C2-F6-1")));
        no5Button.click();
    }

    public void selectN06Button() {
        no6Button.click();
    }


    public void clickYourIdentificationBackButton() {
        YourIdentificationBackButton.click();
    }

    public void clickOverviewButton() {
        overviewButton.click();
    }

}
