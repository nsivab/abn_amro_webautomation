package pageObjects;

import generic.components.UtilityClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.text.ParseException;

public class Confirmation extends UtilityClass {

    public Confirmation(WebDriver driver) throws IOException {
        super();
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 60);
    }

    @FindBy(id = "P580-C2-C0-TI0")
    @CacheLookup
    WebElement confirmationPageTitle;

    @FindBy(id = "P580-C2-C1-B1")
    @CacheLookup
    WebElement applyNowButton;

    @FindBy(xpath = ".//*[@id='P580-C2-C0-TI0']/p")
    @CacheLookup
    WebElement yourBankAccountDetails;

    @FindBy(xpath = ".//*[@class='step-status w-100 d-flex align-items-center ellipsis active']/span[2]")
    @CacheLookup
    WebElement unSelectedConfirmTab;


    public WebElement FinalPageTab(int i) {
        return driver.findElement(By.xpath("(.//*[@class='step-status w-100 d-flex align-items-center ellipsis finished']/span[2])[" + i + "]"));
    }

    public WebElement detailTextVerify(int i) {
        return driver.findElement(By.xpath(".//*[@id='P580-C2-C0-TI5']/span/p/table/tbody/tr[" + i + "]/td"));
    }

    public WebElement pdfLinkVerify(String pdfName) {
        return driver.findElement(By.xpath("//*[@id='P580-C2-C0-TI8']/p/a[text()='" + pdfName + "']"));
    }

    public Boolean getConfirmationPageTitle() {
        return confirmationPageTitle.isDisplayed();
    }

    public void clickApplyNowButton() {
        applyNowButton.click();
    }

    public String nameVerify() {
        return detailTextVerify(3).getText();
    }

    public String addressVerify() {
        return detailTextVerify(6).getText();
    }

    public String dateOfBirthVerify() {
        return detailTextVerify(9).getText();
    }

    public String emailVerify() {
        return detailTextVerify(15).getText();
    }

    public String phoneNumberVerify() {
        return detailTextVerify(12).getText();
    }

    public String packageCostVerify() {
        return yourBankAccountDetails.getText();
    }

    public Boolean pdfLinkClick(String pdfName) {
        wait.until(ExpectedConditions.elementToBeClickable(pdfLinkVerify(pdfName)));
        return true;
    }


    public String FinalPageTabVerify(int pagNo) {
        return FinalPageTab(pagNo).getText();
    }

    public String unSelectedConfirmTabText() {
        return unSelectedConfirmTab.getText();
    }


}
