package pageObjects;

import generic.components.UtilityClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.text.ParseException;

public class ThankYouPage extends UtilityClass {

    public ThankYouPage(WebDriver driver) throws IOException {
        super();
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 60);
    }

    @FindBy(id = "P872-C1-C0-TI2")
    @CacheLookup
    WebElement closingPageTitle;


    public WebElement pageContent() {
        return driver.findElement(By.xpath(".//*[@id='P872-C1-C0-TI2']/p[1]"));
    }

    public Boolean getClosingPageTitle() {
        return closingPageTitle.isDisplayed();
    }

    public String thankYouPageContentVerify() {
        return pageContent().getText();
    }

}
