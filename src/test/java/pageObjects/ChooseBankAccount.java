package pageObjects;


import generic.components.UtilityClass;
import org.openqa.selenium.*;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;


public class ChooseBankAccount extends UtilityClass {

    public ChooseBankAccount(WebDriver driver) throws IOException {
        super();
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 120);

    }

    @FindBy(id = "P391-C2-C1-C0-C0-C1-F1-0")
    @CacheLookup
    WebElement privateButton;

    @FindBy(id = "P391-C2-C1-C0-C0-C1-F1-1")
    @CacheLookup
    WebElement businessButton;

    @FindBy(id = "aab-cookie-consent-agree")
    @CacheLookup
    WebElement acceptCookie;

    @FindBy(id = "P391-C2-C1-C0-C0-C1-F3-0")
    @CacheLookup
    WebElement forMyselfButton;

    @FindBy(id = "P391-C2-C1-C0-C0-C2-C0-C1-F0-0")
    @CacheLookup
    WebElement livingInNetherlandsButton;

    @FindBy(id = "P391-C2-C1-C0-C0-C2-C0-C1-F0-1")
    @CacheLookup
    WebElement livingNotNetherlandsButton;

    @FindBy(id = "P391-C2-C1-C0-C0-C2-C0-C3-C0-C0-F0")
    @CacheLookup
    WebElement postalCode;

    @FindBy(id = "P391-C2-C1-C0-C0-C2-C0-C3-C0-C1-F0")
    @CacheLookup
    WebElement houseNumber;

    @FindBy(id = "P391-C2-C2-B0")
    @CacheLookup
    WebElement tellUsWhoYouAreButton;

    public List<WebElement> cookieElement() {
        return driver.findElements(By.id("aab-cookie-consent-agree"));
    }

    public WebElement streetNameText(String streetName) {
        return driver.findElement(By.xpath(".//p[text()='" + streetName + "']"));
    }

    public WebElement townText(String town) {
        return driver.findElement(By.xpath(".//p[text()='" + town + "']"));
    }

    public void agreeCookie() {
        if (!cookieElement().isEmpty()) {
            acceptCookie.click();
        }
    }

    public void selectPrivate() {
        privateButton.click();
    }

    public void selectBusiness() {
        businessButton.click();
    }

    public void selectMyself() {
        forMyselfButton.click();
    }

    public Boolean selectLivingYes() {
        return livingInNetherlandsButton.isEnabled();
    }

    public void selectLivingInNLNo() {
        livingNotNetherlandsButton.click();
    }

    public void enterPostalCode(String postCode) {
        postalCode.click();
        postalCode.sendKeys(postCode);
        postalCode.sendKeys(Keys.RETURN);
    }

    public void enterHouseNumber(String houseNo) throws InterruptedException {
        waitElementBy(1000);
        houseNumber.click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P391-C2-C1-C0-C0-C2-C0-C3-C0-C1-F0")));
        houseNumber.sendKeys(houseNo);
        houseNumber.sendKeys(Keys.RETURN);
    }

    public Boolean getStreetName(String street) {
        return streetNameText(street).isDisplayed();
    }

    public Boolean getTownName(String town) {
        return townText(town).isDisplayed();
    }

    public String errorText(int index) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//div[@class='ng-active']//span)[" + index + "]"))).getText();
    }

    public String getAlertMessage(int i) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//p[@class='alert-body'])[" + i + "]"))).getText();
    }

    public void clickTellUsWhoYouAre() {
        tellUsWhoYouAreButton.click();
    }

}
