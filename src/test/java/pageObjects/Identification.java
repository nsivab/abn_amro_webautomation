package pageObjects;

import generic.components.UtilityClass;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.text.ParseException;

public class Identification extends UtilityClass {

    public Identification(WebDriver driver) throws IOException {
        super();
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 60);
    }

    @FindBy(id = "P389-C2-C0-C1-TI0")
    @CacheLookup
    WebElement identificationPageTitle;

    @FindBy(id = "P389-C2-C0-C0-F2-2")
    @CacheLookup
    WebElement idProof;

    @FindBy(id = "P389-C2-C0-C1-C1-C1-F0")
    @CacheLookup
    WebElement emailText;

    @FindBy(id = "P389-C2-C0-C1-C1-C2-F0")
    @CacheLookup
    WebElement phoneNumber;

    @FindBy(id = "P389-C2-C1-B1")
    @CacheLookup
    WebElement closingPageButton;

    public WebElement streetNameVerification(String streetName) {
        return driver.findElement(By.xpath(".//*[contains(text(),'" + streetName + "')]"));

    }

    public Boolean getIdentificationPageTitle() {
        return identificationPageTitle.isDisplayed();
    }

    public void enterIdProof() throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P389-C2-C0-C0-F2-2")));
        idProof.click();
        idProof.sendKeys(Keys.RETURN);
    }

    public void enterEmail(String eMail) throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P389-C2-C0-C1-C1-C1-F0")));
        emailText.click();
        emailText.clear();
        emailText.sendKeys(eMail);
        emailText.sendKeys(Keys.RETURN);
    }

    public void enterPhone(String phoneNo) throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P389-C2-C0-C1-C1-C2-F0")));
        phoneNumber.click();
        phoneNumber.clear();
        phoneNumber.sendKeys(phoneNo);
        phoneNumber.sendKeys(Keys.RETURN);
    }

    public void StreetVerify(String streetName) throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[contains(text(),'" + streetName + "')]")));
        streetNameVerification(streetName).isDisplayed();
    }

    public void clickClosingPageButton() {
        closingPageButton.click();
    }

}
