package pageObjects;

import generic.components.UtilityClass;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

public class YourDetails extends UtilityClass {

    public YourDetails(WebDriver driver) throws IOException {
        super();
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 80);
    }

    @FindBy(xpath = "//strong[text()='Your details ']")
    @CacheLookup
    WebElement yourDetailsPageTitle;

    @FindBy(id = "P390-C2-C0-C1-C2-F0-0")
    @CacheLookup
    WebElement salutationButton;

    @FindBy(xpath = ".//*[@class='btn em-enriched-btn ng-pristine ng-untouched ng-valid ng-not-empty active ng-valid-required']")
    @CacheLookup
    WebElement selectedSalutationButton;

    @FindBy(id = "P390-C2-C0-C1-C3-F0")
    @CacheLookup
    WebElement initials;

    @FindBy(id = "P390-C2-C0-C1-C4-F0")
    @CacheLookup
    WebElement firstName;

    @FindBy(id = "P390-C2-C0-C1-C6-C0-F0")
    @CacheLookup
    WebElement surName;

    @FindBy(id = "P390-C2-C0-C1-C8-F0")
    @CacheLookup
    WebElement dateOfBirth;

    @FindBy(id = "P390-C2-C0-C1-F9-0")
    @CacheLookup
    WebElement usaBornYes;

    @FindBy(id = "P390-C2-C0-C1-F9-1")
    @CacheLookup
    WebElement usaBornNo;

    @FindBy(id = "P390-C2-C0-C4-C3-F0")
    @CacheLookup
    WebElement bsnNumber;

    @FindBy(id = "P390-C2-C0-C4-F5-0")
    @CacheLookup
    WebElement taxPayerYes;

    @FindBy(id = "P390-C2-C1-B1")
    @CacheLookup
    WebElement toIdentification;

//    public WebElement errorMessageContent(int index) {
//        return driver.findElement(By.xpath("(//div[@class='invalid-feedback bb-invalid-feedback']/span/span)[" + index + "]"));
//    }

    public WebElement errorMessageContent(String content) {
        return driver.findElement(By.xpath(".//*[contains(@data-translate,'"+content+"')]"));
    }

    public Boolean getPageTitle() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Your details ']")));
        return yourDetailsPageTitle.isDisplayed();
    }

    public void selectSalutation() throws InterruptedException {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P390-C2-C0-C1-C2-F0-0")));
        salutationButton.click();
        salutationButton.sendKeys(Keys.RETURN);

    }

    public void enterInitial(String initial) throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P390-C2-C0-C1-C3-F0")));
        initials.click();
        initials.clear();
        initials.sendKeys(initial);
        initials.sendKeys(Keys.RETURN);
    }

    public void enterFirstname(String fName) throws InterruptedException {
        waitElementBy(1000);
        //wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P390-C2-C0-C1-C4-F0")));
        firstName.click();
        firstName.clear();
        firstName.sendKeys(fName);
        firstName.sendKeys(Keys.RETURN);
    }

    public void enterSurName(String sName) throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P390-C2-C0-C1-C6-C0-F0")));
        surName.click();
        surName.clear();
        surName.sendKeys(String.valueOf(sName));
    }

    public void enterDateOfBirth(String dOb) throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P390-C2-C0-C1-C8-F0")));
        dateOfBirth.click();
        dateOfBirth.sendKeys(dOb);
    }

    public void selectUsaBornNo() throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P390-C2-C0-C1-F9-1")));
        usaBornNo.click();
        usaBornNo.sendKeys(Keys.TAB);
    }

    public void enterBsnNumber(Integer BsnNumber) throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P390-C2-C0-C4-C3-F0")));
        bsnNumber.click();
        bsnNumber.sendKeys(String.valueOf(BsnNumber));
    }

    public void selectTaxPayerYes() throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("P390-C2-C0-C4-F5-0")));
        taxPayerYes.click();
    }

    public void clickToIdentification() throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.visibilityOf(toIdentification));
        toIdentification.click();
    }

    /*public String errorText(int index) {
        wait.until(ExpectedConditions.elementToBeClickable(errorMessageContent(index)));
        return errorMessageContent(index).getText();
    }*/

    public Boolean errorText(String content) throws InterruptedException {
        waitElementBy(1000);
        wait.until(ExpectedConditions.elementToBeClickable(errorMessageContent(content)));
        return errorMessageContent(content).isDisplayed();
    }
}
