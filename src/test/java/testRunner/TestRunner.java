package testRunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;



import java.util.logging.Logger;

import static generic.components.UtilityClass.log;
import static generic.components.UtilityClass.utilityClass;
import static stepDefinitions.OpenMyAccountSteps.driver;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        glue = {"stepDefinitions"},
        features = {"src/test/resources/features/"},
        dryRun = false,
        tags = "@smokeTest",
        plugin = {"pretty", "html:test-report/test-output.html"},
        publish = true
)
public class TestRunner {

    @BeforeClass
    public static void startup() {
        utilityClass.launchBrowser();
    }

    @AfterClass
    public static void teardown(){
        driver.quit();
    }
}
