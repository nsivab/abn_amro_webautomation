package stepDefinitions;

import io.cucumber.java.en.*;
import generic.components.UtilityClass;
import generic.models.*;

import org.junit.Assert;

import pageObjects.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;


public class OpenMyAccountSteps extends UtilityClass {

    public static ChooseBankAccount chooseBankAccount;
    public static YourDetails yourDetails;
    public static Identification identification;
    public static ClosingQuestions closingQuestions;
    public static Confirmation confirmation;
    public static ThankYouPage thankYouPage;

    public UtilityClass utilityClass = new UtilityClass();
    public static String firstName = generateRandomFirstName();
    public static String initial = firstName.substring(0, 1);
    public static String surName = generateRandomLastName();
    public static String email = generateRandomEmail();
    public static String phone = generateRandomPhoneNumber();
    public static String dob;
    public static String baseUrl;

    static {
        try {
            dob = generateRandomBirthDay();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    {
        try {
            baseUrl = utilityClass.readProperties("base.url");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public OpenMyAccountSteps() throws IOException {
    }

    @Given("User opens {string}")
    public void user_opens(String url) throws Exception {
        chooseBankAccount = new ChooseBankAccount(driver);
        yourDetails = new YourDetails(driver);
        identification = new Identification(driver);
        closingQuestions = new ClosingQuestions(driver);
        confirmation = new Confirmation(driver);
        thankYouPage = new ThankYouPage(driver);

        if (url.equalsIgnoreCase("Open Your Account Url")) {
            driver.get(baseUrl);
        } else {
            throw new Exception("Provided url name is invalid");
        }
    }

    @Then("User accept cookie when needed")
    public void user_accept_cookie_when_needed() {
        chooseBankAccount.agreeCookie();
    }

    @When("User select {string} bank Account {string} living {string}")
    public void user_select_bank_Account_living_in(String accountType, String accountFor, String country) {
        if ((accountType.equalsIgnoreCase("Private")) && (accountFor.equalsIgnoreCase("For Myself"))) {
            chooseBankAccount.selectPrivate();
            chooseBankAccount.selectMyself();
        } else if ((accountType.equalsIgnoreCase("Business")) && (accountFor.equalsIgnoreCase("For Myself"))) {
            chooseBankAccount.selectBusiness();
        }

        if ((!country.equalsIgnoreCase("Netherlands"))) {
            chooseBankAccount.selectLivingInNLNo();
        } else if (accountType.equalsIgnoreCase("Private")) {
            Assert.assertTrue("Default living in Netherlands is not selected", chooseBankAccount.selectLivingYes());
        }
    }

    @And("User enter the postalCode {string} and houseNumber {string}")
    public void user_enter_the_postalCode_and_houseNumber(String postalCode, String houseNo) throws InterruptedException {
        chooseBankAccount.enterPostalCode(postalCode);
        chooseBankAccount.enterHouseNumber(houseNo);
    }

    @Then("Verify the streetName {string} and Town {string} is displayed correctly")
    public void verify_the_streetName_and_Town_is_displayed_correctly(String streetName, String townName) {
        Assert.assertTrue("Street Name displayed incorrectly", chooseBankAccount.getStreetName(streetName));
        Assert.assertTrue("Town Name displayed incorrectly", chooseBankAccount.getTownName(townName));
    }

    @When("User Click {string}")
    public void user_click(String nextPageName) throws InterruptedException {
        switch (nextPageName) {
            case "Tell us who you are":
                chooseBankAccount.clickTellUsWhoYouAre();
                break;
            case "Identification":
                yourDetails.clickToIdentification();
                break;
            case "To Closing Questions":
                identification.clickClosingPageButton();
                break;
            case "Overview":
                closingQuestions.clickOverviewButton();
                break;
            case "Apply Now":
                confirmation.clickApplyNowButton();
                break;

            default:
                System.out.println("Invalid Next page Name provided");
        }
    }

    @Then("verify the banner content {string}")
    public void verify_the_banner_content(String banner) {
        Assert.assertEquals("Banner content is not same as expected", BannerContent.valueOf(banner).getBannerDescription(),
                chooseBankAccount.getAlertMessage(BannerContent.valueOf(banner).getBannerId()));
    }

    @Then("Verify that error message {string} is identified in {string} for {string} of {string} page")
    public void verify_that_error_message_is_identified_in_for_of_page(String errorMessage, String errorFlow, String errorFieldInputs, String page) throws InterruptedException {
        String[] errorFields;
        errorFields = errorFieldInputs.split(",");
        for (String errorField : errorFields) {
            if (page.equalsIgnoreCase("ChooseBankAccount")) {
                Assert.assertEquals("Error message is not same as expected",
                        chooseBankAccount.errorText(ErrorField.valueOf(errorField).getFieldId()),
                        ErrorMessage.valueOf(errorMessage).getContent());
            } else if (page.equalsIgnoreCase("yourDetails")) {
                Assert.assertTrue("Error Content is displayed incorrectly",
                        yourDetails.errorText(ErrorMessage.valueOf(errorMessage).getContent()));
            }
        }
    }

//Your Details Step Definitions

    @Then("Verify user is in {string} page")
    public void verify_user_is_in_page(String pageName) {
        switch (pageName) {
            case "Your details":
                Assert.assertTrue("Your details Page is displayed incorrectly", yourDetails.getPageTitle());
                break;
            case "Identification":
                Assert.assertTrue("Identification is displayed incorrectly", identification.getIdentificationPageTitle());
                break;
            case "Closing Questions":
                Assert.assertTrue("Closing Questions is displayed incorrectly", closingQuestions.getClosingPageTitle());
                break;
            case "Confirmation":
                Assert.assertTrue("Confirmation is displayed incorrectly", confirmation.getConfirmationPageTitle());
                break;
            case "Thank You":
                Assert.assertTrue("Thank You is displayed incorrectly", thankYouPage.getClosingPageTitle());
                break;
            default:
                System.out.println("Invalid page Name provided");
        }
    }

    @When("User enters the salutation,initials,firstName,surname and dateOfBirth")
    public void user_enters_the_salutation_initials_first_name_surname_and_date_of_birth() throws InterruptedException {
        yourDetails.selectSalutation();
        yourDetails.enterInitial(initial);
        yourDetails.enterFirstname(firstName);
        yourDetails.enterSurName(surName);
        yourDetails.enterDateOfBirth(dob);
    }

    @When("User not select born in USA")
    public void user_not_select_born_in_usa() throws InterruptedException {
        yourDetails.selectUsaBornNo();
    }

    @When("User enter {string} BSN {int} and Netherlands tax payer {string}")
    public void user_enter_bsn_and_netherlands_tax_payer(String bsnValid, Integer bsnNumber, String taxPayer) throws InterruptedException {

        if (bsnValid.equalsIgnoreCase("valid") && taxPayer.equalsIgnoreCase("Yes")) {
            yourDetails.enterBsnNumber(bsnNumber);
            yourDetails.selectTaxPayerYes();
        }
    }

    @When("User enter {string} initials {string}, dob {string} and dutch personal id {string}")
    public void user_enter_initials_dob_and_dutch_personal_id(String errorFlow, String initials, String dateOfBirth, String dutchId) throws InterruptedException {
        yourDetails.selectSalutation();
        yourDetails.enterInitial(initials);
        yourDetails.enterFirstname(firstName);
        yourDetails.enterSurName(surName);
        yourDetails.enterDateOfBirth(dateOfBirth);
        yourDetails.selectUsaBornNo();
        yourDetails.enterBsnNumber(Integer.valueOf(dutchId));
        yourDetails.selectTaxPayerYes();
    }

    //    Identification StepDefinitions
    @When("User selects the {string} ID proof and given {string} Email Phone Number")
    public void user_selects_the_ID_proof_and_given_Email_Phone_Number(String idProof, String emailPhoneNo) throws InterruptedException {
        if (idProof.equalsIgnoreCase("Valid") && emailPhoneNo.equalsIgnoreCase("Valid")) {
            identification.enterIdProof();
            identification.enterEmail(email);
            identification.enterPhone(phone);
        }
    }

    @When("User checks streetName given in Previous page {string}")
    public void user_checks_street_name_given_in_previous_page(String streetName) throws InterruptedException {
        identification.StreetVerify(streetName);
    }

    //    Closing Questions
    @Then("User {string} response to {string} income questions")
    public void user_response_to_income_questions(String response, String incomeType) throws InterruptedException {
        switch (response) {
            case "Valid":
                if (incomeType.equalsIgnoreCase("Not applicable") || incomeType.equalsIgnoreCase("Cryptocurrency")) {
                    closingQuestions.selectNACheckbox();
                    closingQuestions.selectYes2Button();
                    closingQuestions.selectN03Button();
                    closingQuestions.selectN04Button();
                } else {
                    closingQuestions.selectRealEstateCheckbox();
                    closingQuestions.selectN01Button();
                    closingQuestions.selectYes2Button();
                    closingQuestions.selectN03Button();
                    closingQuestions.selectN04Button();
                    closingQuestions.selectN05Button();
                    closingQuestions.selectN06Button();
                }
                break;
            case "Some Blank":
                Assert.assertTrue("Identification is displayed incorrectly", identification.getIdentificationPageTitle());
                break;
            case "Never":
                Assert.assertTrue("Closing Questions is displayed incorrectly", closingQuestions.getClosingPageTitle());
                break;
            default:
                System.out.println("Invalid page Name provided");
        }

    }

    //    Confirmation Steps
    @Then("Verify the name")
    public void verify_the_name() {
        Assert.assertEquals("User name is not same as expected", confirmation.nameVerify().replaceAll("[^a-zA-Z0-9]", ""), "Mr" + initial + firstName + surName);
    }

    @Then("Verify the Address is {string}")
    public void verify_the_address_is(String address) {
        Assert.assertEquals("Address is not same as expected", confirmation.addressVerify().replaceAll("[^a-zA-Z0-9]", ""), address.replaceAll("[^a-zA-Z0-9]", ""));
    }

    @Then("Verify the Date of Birth")
    public void verify_the_date_of_birth() {
        Assert.assertEquals("Date of birth is not same as expected", confirmation.dateOfBirthVerify(), dob);
    }

    @Then("Verify the TelephoneNumber")
    public void verify_the_telephone_number() {
        Assert.assertEquals("Phonenumber is not same as expected", confirmation.phoneNumberVerify(), phone);
    }

    @Then("Verify the EmailAddress")
    public void verify_the_email_address() {
        Assert.assertEquals("EmailAddress is not same as expected", confirmation.emailVerify().replaceAll("[^a-zA-Z0-9]", ""), email.replaceAll("[^a-zA-Z0-9]", ""));
    }

    @Then("Verify the Basic package cost is {string}")
    public void verify_the_basic_package_cost_is(String packageCost) {
        Assert.assertTrue("Package cost is not same as expected", confirmation.packageCostVerify().contains(packageCost));
    }

    @Then("PDF links are verified for {string}")
    public void pdf_links_are_verified_for(String pdfNames) {
        String[] pdfEnumNames = pdfNames.split(",");
        for (String pdfEnumName : pdfEnumNames) {
            Assert.assertTrue("Pdf " + PdfLinksInfo.valueOf(pdfEnumName).getPdfLink() + "is confirmed", confirmation.pdfLinkClick(PdfLinksInfo.valueOf(pdfEnumName).getPdfLink()));
        }
    }

    @And("Verify the {string} screens have highlighted texts")
    public void verify_the_Tab_name_and_selected_text(String tabInfo) {
        String[] tabName = tabInfo.split(",");
        HashMap<Integer, String> tabNames = new HashMap<Integer, String>();
        for (String text : tabName) {
            tabNames.put(TabContent.valueOf(text).getTabId(), TabContent.valueOf(text).getTabText());
        }
        for (Integer tab : tabNames.keySet()) {
            if (tab == TabContent.valueOf("CONFIRMATION").getTabId()) {
                Assert.assertEquals("Tab Name is not same as expected", confirmation.unSelectedConfirmTabText(), tabNames.get(tab));
            } else {
                Assert.assertEquals("Tab Name is not same as expected", confirmation.FinalPageTabVerify(tab), tabNames.get(tab));
            }
        }
    }

    //    Thank you page
    @Then("User verifies confirmation is send to correct email")
    public void user_verifies_confirmation_is_send_to_correct_email() {
        Assert.assertTrue("Email confirmation is not sent to the right email", thankYouPage.thankYouPageContentVerify().contains(email));
    }

    @Given("User close the browser")
    public void user_close_the_browser() {
        try {
            driver.quit();
            driver.close();
        } catch (Exception e) {
        }
    }
}
