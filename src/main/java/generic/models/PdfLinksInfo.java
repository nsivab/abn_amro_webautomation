package generic.models;

public enum PdfLinksInfo {

    BASIC_PAYMENT("Basic Payment Package conditions", "Basic_Payment_Package"),
    RETAIL_PAYMENT("Retail Payment Information Sheet", "Payment_Services_Information_Sheet"),
    DEPOSITOR("Depositor Information Template", "Depositor-Information-Template");

    String pdfLink;
    String pdfUrlContent;

    public String getPdfLink() {
        return pdfLink;
    }

    public String getPdfUrlContent() {
        return pdfUrlContent;
    }

    PdfLinksInfo(String link, String urlContent) {
        pdfLink = link;
        pdfUrlContent = urlContent;
    }
}
