package generic.models;

public enum ErrorField {

    POSTAL_CODE(1),
    HOUSE_NUMBER(4),
    INVALID_INITIALS(2),
    FUTURE_DOB(2),
    AGE_DOB(2),
    INVALID_PERSONAL_ID(2),
    BLANK_INITIALS(2),
    BLANK_DOB(6),
    BLANK_PERSONAL_ID(9);

    int fieldId;
    public Integer getFieldId() {
        return Integer.valueOf(fieldId);
    }

    ErrorField(int id) {
        fieldId = id;
    }
}
