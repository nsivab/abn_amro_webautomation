package generic.models;

public enum ErrorMessage {

    BLANK_ERROR_MESSAGE("Fill in an answer to continue."),
    INVALID_POSTAL_CODE("We do not recognise your postcode. Please check that it has been entered correctly."),
    INVALID_HOUSE_N0("Use only numbers."),
    INVALID_INITIAL_ERROR("You can enter initials up to 5 letters."),
    INVALID_DOB_ERROR("Enter a date in the past.A payment package can be requested by people over the age of 18."),
    INVALID_DUTCH_ID("The burgerservicenummer [Dutch personal ID number] seems to be invalid. Please check that it has been entered correctly."),
    INVALID_AGE_ERROR("A payment package can be requested by people over the age of 18.");



    private final String content;

    ErrorMessage(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
