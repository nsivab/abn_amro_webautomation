package generic.models;

public enum BannerContent {

    PRIVACY_BANNER_CHOOSE_BANK_ACC(1, "We will use the personal data you enter to process your application. To find out more about how we process your personal data, please read our privacy statement."),
    INFO_BANNER_CHOOSE_BANK_ACC(2, "Our bank account is called Basic Payment Package, which you can use to do your day-to-day banking. You can also easily open a savings account or apply for a credit card."),
    NOT_IN_NETHERLANDS(3,"If no, please call us at +31 (0)20 343 40 02 (Monday to Friday from 8am - 5.30 pm)."),
    BUSINESS_ACCOUNT(2,"If you would like to open a business account, you can do so easily online."),
    INFO_BANNER_YOUR_DETAILS(1,"Please check your full name (name and surname) as stated on your proof of identity so that your application can be processed quickly.");

    int bannerId;
    String bannerDescription;

    public Integer getBannerId() {
        return Integer.valueOf(bannerId);
    }

    public String getBannerDescription() {
        return bannerDescription;
    }

    BannerContent(int id, String description) {
        bannerId = id;
        bannerDescription = description;
    }
}
