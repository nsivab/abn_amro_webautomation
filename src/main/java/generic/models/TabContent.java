package generic.models;

public enum TabContent {

    CHOOSE_BANK_ACC(1, "Choose bank account"),
    YOUR_DETAILS(2, "Your details"),
    IDENTIFICATION(3,"Identifying"),
    CLOSING_QUESTIONS(4,"Closing questions"),
    CONFIRMATION(5,"Confirm");

    int tabId;
    String tabText;

    public Integer getTabId() {
        return tabId;
    }

    public String getTabText() {
        return tabText;
    }

    TabContent(int id, String text) {
        tabId = id;
        tabText = text;
    }
}
