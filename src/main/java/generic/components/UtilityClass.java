package generic.components;


import com.github.javafaker.Faker;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class UtilityClass {
    public static WebDriver driver;
    public static WebDriverWait wait;
    public static Faker faker = new Faker(new Locale("en-GB"));
    public static UtilityClass utilityClass;
    public static String browser;
    public static Logger log = LogManager.getLogger("devpinoyLogger");

    static {
        try {
            utilityClass = new UtilityClass();
            browser = utilityClass.readProperties("browser.name");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public UtilityClass() throws IOException {
    }

    public String readProperties(String properties) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(new File("src/test/resources/properties/base.properties").getAbsolutePath());
        Properties properties1 = new Properties();
        properties1.load(fileInputStream);
        fileInputStream.close();
        return properties1.getProperty(properties);

    }

    public static void launchBrowser(){
        driver = openBrowser(driver);
    }

    public static WebDriver openBrowser(WebDriver driver) {

        switch (browser) {
            case "Chrome":
                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless");
                options.addArguments("--no-sandbox");
                options.addArguments("--disable-dev-shm-usage");
                options.addArguments("--disable-popup-blocking");
                options.addArguments("--disable-gpu");
                options.addArguments("--window-size=1400,800");
                options.setPageLoadStrategy(PageLoadStrategy.EAGER);
                driver = new ChromeDriver(options);
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                System.out.println("Chrome launched successfully");
                break;
            case "Firefox":
                WebDriverManager.firefoxdriver().setup();
                FirefoxOptions FFOptions = new FirefoxOptions();
                FFOptions.addArguments("--headless");
                FFOptions.addArguments("--no-sandbox");
                FFOptions.addArguments("--disable-dev-shm-usage");
                FFOptions.addArguments("--disable-gpu");
                FFOptions.addArguments("--window-size=1400,800");
                driver = new FirefoxDriver(FFOptions);
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                driver.manage().window().maximize();
                break;
            default:
                System.out.println("Invalid Browser Name provided");
                break;
        }
        return driver;
    }

    public static String generateRandomFirstName() {
        return faker.name().firstName();
    }

    public static String generateRandomLastName() {
        return faker.name().lastName();
    }

    public static Integer generateRandomInteger(Integer min, Integer max) {
        Random r = new Random();
        return r.nextInt(max - min) + max;
    }

    public static String generateRandomEmail() {
        return faker.internet().emailAddress();
    }

    public static String generateRandomPhoneNumber() {

        return "+3168" + generateRandomInteger(1000000, 9999999);
    }

    public static String generateRandomBirthDay() throws ParseException {
        Random random = new Random();
        int minDay = (int) LocalDate.of(1960, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(2000, 12, 30).toEpochDay();
        long randomDay = minDay + random.nextInt(maxDay - minDay);

        LocalDate randomBirthDate = LocalDate.ofEpochDay(randomDay);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date d = sdf.parse(String.valueOf(randomBirthDate));
        sdf.applyPattern("dd-MM-yyyy");
        return sdf.format(d);
    }

    public void waitElementBy(int time) throws InterruptedException {
        Thread.sleep(time);
    }


}
